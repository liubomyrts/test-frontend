# test-front

> A Vue.js project

## Build Setup

install node v.8.10.0> 
npm v.3.5.2>
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
