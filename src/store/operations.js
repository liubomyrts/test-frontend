import data from './data.js'
import city from './city.js'
import category from './category.js'

export default {
  state: {
    data,
    city,
    category,
    filters: data
  },
  getters: {
      products(state) {
        return state.data
      },
      filteredProducts(state) {
        return state.filters
      },
      cities(state) {
        return state.city
      },
      categories(state) {
        return state.category
      },
      findCategory: state => id => {
        return state.category.find(category => category.id == id).name
      },
      findCity: state => id => {
        return state.city.find(category => category.id == id).name
      }
  },
  mutations: {
    q(state,  credentials) {
      let selCity = state.city.find((item) => item.name === credentials.cities);
      state.filters = state.data.filter((item) => item.city == selCity.id && item.price > credentials.prices[0] && item.price < credentials.prices[1] && credentials.categories.indexOf(item.category) >= 0);
    }
  }
}
